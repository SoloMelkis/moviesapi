package com.solomon.MoviesList.util;

public class Constants {

    public static final String BASE_URL = "http://www.omdbapi.com";
    public static final int NETWORK_TIMEOUT = 3000;


    public static final String API_KEY = "a1e74aca";

    public static final String[] DEFAULT_SEARCH_CATEGORY_IMAGES =
            {
                    "marvel",
                    "dc",
                    "thriller",
                    "sports",
                    "comedy"


            };
    public static final String[] DEFAULT_SEARCH_CATEGORY =
            {
                    "MARVEL",
                    "BATMAN",
                    "THRILLER",
                    "SPORTS",
                    "COMEDY"


            };

}
