package com.solomon.MoviesList;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.solomon.MoviesList.models.Movie;
import com.solomon.MoviesList.viewModels.MovieViewModel;

public class MovieActivity extends BaseActivity {
    private static final String TAG = "MovieActivity";
    private AppCompatImageView mMovieImage;
    private TextView mMovieTitle, mDirector, mActor, mIMDBScore, mYear;
    private TextView mDescription;
    private ScrollView mScrollView;
    private MovieViewModel mMovieViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        mMovieImage = findViewById(R.id.movie_image);
        mMovieTitle = findViewById(R.id.movie_title);
        mDirector = findViewById(R.id.director_title);
        mActor = findViewById(R.id.actors);
        mIMDBScore = findViewById(R.id.imdb_score);
        mDescription = findViewById(R.id.description_container);
        //mYear = findViewById(R.id.year);
        mScrollView = findViewById(R.id.parent);

        mMovieViewModel = ViewModelProviders.of(this).get(MovieViewModel.class);
        showProgressBar(true);
        subscribeObservers();
        getIncomingIntent();
    }

    private void getIncomingIntent() {
        if (getIntent().hasExtra("movie")) {
            Movie movie = getIntent().getParcelableExtra("movie");
            Log.d(TAG, "getIncomingIntent: " + movie.getTitle());
            mMovieViewModel.searchMovieByTitle(movie.getTitle());

        }
    }

    private void subscribeObservers() {
        mMovieViewModel.getMovie().observe(this, new Observer<Movie>() {
            @Override
            public void onChanged(@Nullable Movie movie) {
                if (movie != null) {
                    setMovieProperties(movie);

                }
            }
        });
    }

    private void setMovieProperties(Movie movie) {
        if (movie != null) {
            Log.d(TAG, "setMovieProperties: "+movie);

            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(R.drawable.ic_launcher_background);

            Glide.with(this)
                    .setDefaultRequestOptions(requestOptions)
                    .load(movie.getPoster())
                    .into(mMovieImage);
            mMovieTitle.setText(movie.getTitle());

            mActor.setText(movie.getActors());
            mIMDBScore.setText(movie.getImdbRating());
            mDescription.setText(movie.getPlot());
            //mYear.setText(movie.getYear());
            mDirector.setText(movie.getDirector());
        }
        showParent();

        showProgressBar(false);
    }


    private void showParent() {
        mScrollView.setVisibility(View.VISIBLE);
    }
}



