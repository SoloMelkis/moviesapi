package com.solomon.MoviesList.repository;

import android.arch.lifecycle.LiveData;

import com.solomon.MoviesList.models.Movie;
import com.solomon.MoviesList.requests.MovieApiClient;

import java.util.List;

public class MovieRepository {

    private static MovieRepository instance;
    private MovieApiClient mMovieApliClient;
    private String mQuery;
    private int mPageNumber;

    public static MovieRepository getInstance(){
        if(instance ==null){
            instance = new MovieRepository();

        }return instance;
    }
    private MovieRepository(){
    mMovieApliClient = MovieApiClient.getInstance();
    }
    public LiveData<List<Movie>> getMovies(){
        return mMovieApliClient.getMovies();
    }
    public LiveData<Movie> getMovie(){
        return mMovieApliClient.getMovie();
    }

    public void searchMoviesApi(String query,int pageNumber){
        if(pageNumber == 0){
            pageNumber = 1;
        }
        mQuery = query;
        mPageNumber = pageNumber;
        mMovieApliClient.searchMoviesApi(query, pageNumber);
    }

    public void searchMovieByTitle(String movieTitle){
        mMovieApliClient.searchMovieByTitle(movieTitle);
    }

    public void searchNextPage(){
        searchMoviesApi(mQuery, mPageNumber+1);
    }
}
