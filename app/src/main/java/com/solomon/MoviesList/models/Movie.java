package com.solomon.MoviesList.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;

public class Movie implements Parcelable{

    private String Title;
    private String Plot;
    private String Director;
    private String Actors;
    private String imdbRating;
    private String Poster;
    private String Year;

    public Movie(){

    }

    public Movie(String title, String plot, String director, String actors, String imdbRating, String poster, String year) {
        Title = title;
        Plot = plot;
        Director = director;
        Actors = actors;
        this.imdbRating = imdbRating;
        Poster = poster;
        Year = year;
    }

    protected Movie(Parcel in) {
        Title = in.readString();
        Plot = in.readString();
        Director = in.readString();
        Actors = in.readString();
        imdbRating = in.readString();
        Poster = in.readString();
        Year = in.readString();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getPlot() {
        return Plot;
    }

    public void setPlot(String plot) {
        Plot = plot;
    }

    public String getDirector() {
        return Director;
    }

    public void setDirector(String director) {
        Director = director;
    }

    public String getActors() {
        return Actors;
    }

    public void setActors(String actors) {
        Actors = actors;
    }

    public String getImdbRating() {
        return imdbRating;
    }

    public void setImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
    }

    public String getPoster() {
        return Poster;
    }

    public void setPoster(String poster) {
        Poster = poster;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Title);
        dest.writeString(Plot);
        dest.writeString(Director);
        dest.writeString(Actors);
        dest.writeString(imdbRating);
        dest.writeString(Poster);
        dest.writeString(Year);
    }

    @Override
    public String toString() {
        return "Movie{" +
                "Title='" + Title + '\'' +
                ", Plot='" + Plot + '\'' +
                ", Director='" + Director + '\'' +
                ", Actors='" + Actors + '\'' +
                ", imdbRating='" + imdbRating + '\'' +
                ", Poster='" + Poster + '\'' +
                ", Year='" + Year + '\'' +
                '}';
    }
}
