package com.solomon.MoviesList.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.solomon.MoviesList.models.Movie;
import com.solomon.MoviesList.repository.MovieRepository;

public class MovieViewModel extends ViewModel {

    private MovieRepository mMovieRepository;

    public MovieViewModel(){
        mMovieRepository = MovieRepository.getInstance();

    }
    public LiveData<Movie> getMovie(){
        return mMovieRepository.getMovie();
    }

    public void searchMovieByTitle(String movieTitle){
        mMovieRepository.searchMovieByTitle(movieTitle);

    }
}
