package com.solomon.MoviesList.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.solomon.MoviesList.models.Movie;
import com.solomon.MoviesList.repository.MovieRepository;

import java.util.List;

public class MovieListViewModel extends ViewModel {

    private boolean mIsViewingMovies;
    private MovieRepository mMovieRepository;

    public MovieListViewModel() {
        mIsViewingMovies  =false;
        mMovieRepository = MovieRepository.getInstance();
    }

    public LiveData<List<Movie>> getMovies() {
        return mMovieRepository.getMovies();
    }

    public void searchMoviesApi(String query, int pageNumber) {
        mIsViewingMovies = true;
        mMovieRepository.searchMoviesApi(query, pageNumber);
    }

    public void searchNextPage(){
        if(mIsViewingMovies){
            mMovieRepository.searchNextPage();
        }
    }

    public boolean isViewingMovies(){
        return mIsViewingMovies;
    }

    public void setIsViewingMovies(boolean isViewingMovies){
    mIsViewingMovies = isViewingMovies;
    }

    public boolean onBackPressed(){
        if(mIsViewingMovies){
            mIsViewingMovies = false;
            return false;
        }
        return true;
    }
}
