package com.solomon.MoviesList;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;

import com.solomon.MoviesList.adapters.MovieRecyclerAdapter;
import com.solomon.MoviesList.adapters.OnMovieListener;
import com.solomon.MoviesList.models.Movie;
import com.solomon.MoviesList.requests.MovieApi;
import com.solomon.MoviesList.requests.ServiceGenerator;
import com.solomon.MoviesList.requests.responses.MovieResponse;
import com.solomon.MoviesList.requests.responses.MovieSearchResponse;
import com.solomon.MoviesList.util.Constants;
import com.solomon.MoviesList.util.Testing;
import com.solomon.MoviesList.util.VerticalSpacingItemDecorator;
import com.solomon.MoviesList.viewModels.MovieListViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MovieListActivity extends BaseActivity implements OnMovieListener {

    private static final String TAG = "MovieListActivity";
    private MovieListViewModel mMovieListViewModel;
    private RecyclerView mRecyclerView;
    private MovieRecyclerAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);
        mRecyclerView = findViewById(R.id.movie_list);
        mMovieListViewModel = ViewModelProviders.of(this).get(MovieListViewModel.class);
        initRecyclerView();
        subscribeObservers();
        initSearchView();
        if (!mMovieListViewModel.isViewingMovies()) {
            displayCategories();
        }

    }

    private void subscribeObservers() {
        mMovieListViewModel.getMovies().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(@Nullable List<Movie> movies) {
                if (movies != null) {
                    if(mMovieListViewModel.isViewingMovies()){
                    Testing.printMovies(movies, "movie set");
                    mAdapter.setMovies(movies);
                }}

            }
        });
    }

    private void initRecyclerView() {
        mAdapter = new MovieRecyclerAdapter(this);
        VerticalSpacingItemDecorator itemDecorator = new VerticalSpacingItemDecorator(30);
        mRecyclerView.addItemDecoration(itemDecorator);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if(!mRecyclerView.canScrollVertically(1)){
                    //load data from next page
                    mMovieListViewModel.searchNextPage();
                }
            }
        });
    }


    private void initSearchView() {
        final SearchView searchView = findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                mAdapter.displayLoading();
                mMovieListViewModel.searchMoviesApi(s, 1);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }




    @Override
    public void onCategoryClick(String category) {
        mAdapter.displayLoading();
        mMovieListViewModel.searchMoviesApi(category, 1);
    }

    @Override
    public void onMovieClick(int position) {
        Intent intent = new Intent(this, MovieActivity.class);
        intent.putExtra("movie", mAdapter.getSelectedMovie(position));

        startActivity(intent);}

    private void displayCategories() {
        mMovieListViewModel.setIsViewingMovies(false);
        mAdapter.searchCategory();
    }

    @Override
    public void onBackPressed() {
        if (mMovieListViewModel.onBackPressed()) {
            super.onBackPressed();
        } else {
            displayCategories();
        }
    }
}

