package com.solomon.MoviesList.adapters;

public interface OnMovieListener {

    void onMovieClick(int position);

    void onCategoryClick(String category);
}
