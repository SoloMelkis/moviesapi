package com.solomon.MoviesList.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.solomon.MoviesList.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    CircleImageView category_image;
    TextView category_title;
    OnMovieListener onMovieListener;
    public CategoryViewHolder(@NonNull View itemView, OnMovieListener onMovieListener) {
        super(itemView);

        this.onMovieListener = onMovieListener;
        //category_image = itemView.findViewById(R.id.);
        category_title = itemView.findViewById(R.id.category_title);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
    onMovieListener.onCategoryClick(category_title.getText().toString());
    }
}
