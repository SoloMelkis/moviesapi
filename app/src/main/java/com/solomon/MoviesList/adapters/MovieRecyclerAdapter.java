package com.solomon.MoviesList.adapters;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.solomon.MoviesList.R;
import com.solomon.MoviesList.models.Movie;
import com.solomon.MoviesList.util.Constants;

import java.util.ArrayList;
import java.util.List;

public class MovieRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int MOVIE_TYPE = 1;
    private static final int Loading_TYPE = 2;
    private static final int CATEGORY_TYPE = 3;

    private List<Movie> mMovies;
    private OnMovieListener mOnMOvieListener;

    public MovieRecyclerAdapter(OnMovieListener onMovieListener) {
        mOnMOvieListener = onMovieListener;
        mMovies = new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = null;
        switch (i){
            case MOVIE_TYPE:{
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_movie_list, viewGroup, false);
                return new MovieViewHolder(view, mOnMOvieListener);
            }

            case Loading_TYPE:{
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_loading_list, viewGroup, false);
                return new LoadingViewHolder(view);
            }
            case CATEGORY_TYPE:{
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_category_list, viewGroup, false);
                return new CategoryViewHolder(view, mOnMOvieListener);
            }

            default:{
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_movie_list, viewGroup, false);
                return new MovieViewHolder(view, mOnMOvieListener);
            }
        }


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        int itemViewType = getItemViewType(i);
        if(itemViewType == MOVIE_TYPE) {
            // set the image
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .error(R.drawable.ic_launcher_background);

            Glide.with(((MovieViewHolder) viewHolder).itemView)
                    .setDefaultRequestOptions(options)
                    .load(mMovies.get(i).getPoster())
                    .into(((MovieViewHolder)viewHolder).image);

            ((MovieViewHolder)viewHolder).title.setText(mMovies.get(i).getTitle());
        }
        else if(itemViewType == CATEGORY_TYPE) {
            // set the image
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .error(R.drawable.ic_launcher_background);
            Uri path = Uri.parse("android.resource://com.solomon.MoviesList/drawable/" + mMovies.get(i).getPoster());
            Glide.with(((CategoryViewHolder) viewHolder).itemView);


            ((CategoryViewHolder)viewHolder).category_title.setText(mMovies.get(i).getTitle());
        }
    }

    @Override
    public int getItemViewType(int position) {

        if(mMovies.get(position).getYear() == "null")
            return CATEGORY_TYPE;
        else if(mMovies.get(position).getTitle().equals("Loading...")){
            return Loading_TYPE;
        }
        else if(position == mMovies.size()-1
                && position!=0
                && !mMovies.get(position).getTitle().equals("EXHAUSTED...")){
            return Loading_TYPE;
        }

        else {return MOVIE_TYPE;}}

    @Override
    public int getItemCount() {
        return mMovies.size();
    }

    public void displayLoading(){
        if(!isLoading()){
            Movie movie = new Movie();
            movie.setTitle("Loading...");
            List<Movie> loadingList = new ArrayList<>();
            loadingList.add(movie);
            mMovies = loadingList;
            notifyDataSetChanged();
        }
    }

    public void searchCategory(){
        List<Movie> categories = new ArrayList<>();
        for(int i=0; i< Constants.DEFAULT_SEARCH_CATEGORY.length; i++){
            Movie movie = new Movie();
            movie.setTitle(Constants.DEFAULT_SEARCH_CATEGORY[i]);
            movie.setPoster(Constants.DEFAULT_SEARCH_CATEGORY_IMAGES[i]);
            movie.setYear("null");
            categories.add(movie);
        }
        mMovies = categories;
        notifyDataSetChanged();
    }

    private boolean isLoading(){
        if(mMovies.size() > 0){
            if(mMovies.get(mMovies.size() - 1).getTitle().equals("LOADING...")){
                return true;
            }
        }
        return false;
    }

    public void setMovies(List<Movie> movies){
        mMovies = movies;
        notifyDataSetChanged();
    }

    public Movie getSelectedMovie(int position){
        if(mMovies!=null){
            if(mMovies.size()>0){
                return mMovies.get(position);
            }
        }
        return null;
    }
}