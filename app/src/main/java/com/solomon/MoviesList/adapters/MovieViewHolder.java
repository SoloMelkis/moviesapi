package com.solomon.MoviesList.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.solomon.MoviesList.R;

public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView title, director, imdb_score;
    AppCompatImageView image;
    OnMovieListener onMovieListener;

    public MovieViewHolder(@NonNull View itemView, OnMovieListener onMovieListener) {
        super(itemView);
        this.onMovieListener = onMovieListener;
        title = itemView.findViewById(R.id.movie_title);
        director = itemView.findViewById(R.id.movie_director);
//        imdb_score = itemView.findViewById(R.id.movie_imdb_score);
      image = itemView.findViewById(R.id.movie_image);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
    onMovieListener.onMovieClick(getAdapterPosition());
    }
}
