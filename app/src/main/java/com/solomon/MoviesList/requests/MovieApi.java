package com.solomon.MoviesList.requests;

import com.solomon.MoviesList.requests.responses.MovieResponse;
import com.solomon.MoviesList.requests.responses.MovieSearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MovieApi {

    // SEARCH
    @GET(".")
    Call<MovieSearchResponse> searchMovie(
            @Query("apikey") String key,
            @Query("s") String query,
            @Query("page") String page
    );

    // GET SPECIFIC MOVIE
    @GET(".")
    Call<MovieResponse> movieSearch(
            @Query("apikey") String key,
            @Query("t") String movieTitle
    );

}
