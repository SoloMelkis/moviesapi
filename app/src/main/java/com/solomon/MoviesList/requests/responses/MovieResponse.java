package com.solomon.MoviesList.requests.responses;

import com.solomon.MoviesList.models.Movie;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieResponse {

    @SerializedName("Title")
    @Expose
    private String Title;
    public String getTitle() {
        return Title;
    }

    @SerializedName("Director")
    @Expose
    private String Director;
    public String getDirector() {
        return Director;
    }

    @SerializedName("Actors")
    @Expose
    private String Actors;
    public String getActors() {
        return Actors;
    }

    @SerializedName("Plot")
    @Expose
    private String Plot;
    public String getPlot() {
        return Plot;
    }

    @SerializedName("imdbRating")
    @Expose
    private String imdbRating;
    public String getImdbRating() {
        return imdbRating;
    }

    @SerializedName("Poster")
    @Expose
    private String Poster;
    public String getPoster() {
        return Poster;
    }

    @SerializedName("Year")
    @Expose
    private String Year;
    public String getYear(){ return Year; }



//    private String Movie;
//    private String getMovie(){
//        return Movie;
//    }


    @Override
    public String toString() {
        return "MovieResponse{" +
                "Title='" + Title + '\'' +
                ", Director='" + Director + '\'' +
                ", Actors='" + Actors + '\'' +
                ", Plot='" + Plot + '\'' +
                ", imdbRating='" + imdbRating + '\'' +
                ", Poster='" + Poster + '\'' +
                ", Year='" + Year + '\'' +
                '}';
    }
}
