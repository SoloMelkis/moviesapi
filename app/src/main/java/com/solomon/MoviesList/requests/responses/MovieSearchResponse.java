package com.solomon.MoviesList.requests.responses;

import com.solomon.MoviesList.models.Movie;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieSearchResponse {

    @SerializedName("totalResults")
    @Expose
    private int totalResults;
    public int getCount() {
        return totalResults;
    }

    @SerializedName("Search")
    @Expose
    private List<Movie> Search;
    public List<Movie> getSearch() {
        return Search;
    }

    @Override
    public String toString() {
        return "MovieSearchResponse{" +
                "totalResults=" + totalResults +
                ", Search=" + Search +
                '}';
    }
}
