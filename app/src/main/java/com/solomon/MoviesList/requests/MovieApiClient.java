package com.solomon.MoviesList.requests;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.solomon.MoviesList.AppExecutors;
import com.solomon.MoviesList.models.Movie;
import com.solomon.MoviesList.requests.responses.MovieResponse;
import com.solomon.MoviesList.requests.responses.MovieSearchResponse;
import com.solomon.MoviesList.util.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;

import static com.solomon.MoviesList.util.Constants.NETWORK_TIMEOUT;

public class MovieApiClient {
    private static final String TAG="MovieApiClient";
    private RetrieveMoviesRunnable mRetrieveMoviesRunnable;
    private RetrieveMovieRunnable mRetrieveMovieRunnable;


    private static MovieApiClient instance;
    private MutableLiveData<List<Movie>> mMovies;
    private MutableLiveData<Movie> mMovie;



    public static MovieApiClient getInstance(){
        if(instance == null){
            instance = new MovieApiClient();
        }return instance;
    }

    private MovieApiClient(){
        //instantiation of objects
        mMovies = new MutableLiveData<>();
        mMovie = new MutableLiveData<>();


    }
    public LiveData<List<Movie>> getMovies(){
        return mMovies;
    }

    public LiveData<Movie> getMovie(){
        return mMovie;
    }



    public void searchMoviesApi(String query, int pageNumber){
        if(mRetrieveMoviesRunnable !=null){
            mRetrieveMoviesRunnable = null;
        }
        mRetrieveMoviesRunnable = new RetrieveMoviesRunnable(query, pageNumber);
        final Future handler = AppExecutors.getInstance().networkIO().submit(mRetrieveMoviesRunnable);

        AppExecutors.getInstance().networkIO().schedule(new Runnable() {
            @Override
            public void run() {
            handler.cancel(true);
            }
        }, NETWORK_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    public void searchMovieByTitle(String movieTitle){
        if(mRetrieveMovieRunnable !=null){
            mRetrieveMovieRunnable = null;
        }
        mRetrieveMovieRunnable = new RetrieveMovieRunnable(movieTitle);
        final Future handler = AppExecutors.getInstance().networkIO().submit(mRetrieveMovieRunnable);

        AppExecutors.getInstance().networkIO().schedule(new Runnable() {
            @Override
            public void run() {
                handler.cancel(true);
            }
        }, NETWORK_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    private class RetrieveMoviesRunnable implements Runnable{
        private String query;
        private int pageNumber;
        boolean cancelRequest;

        public RetrieveMoviesRunnable(String query, int pageNumber) {
            this.query = query;
            this.pageNumber = pageNumber;
            cancelRequest = false;

        }

        @Override
        public void run() {
            try {
                Response response = getSearch(query,pageNumber).execute();
                if(cancelRequest){return;}

            if (response.code() == 200){
                List<Movie> list = new ArrayList<>(((MovieSearchResponse)response.body()).getSearch());
                if(pageNumber == 1){
                    mMovies.postValue(list);
                }
                else{
                    List<Movie> currentMovies = mMovies.getValue();
                    currentMovies.addAll(list);
                    mMovies.postValue(currentMovies);
                }
            }else{
                String error = response.errorBody().string();
                Log.e(TAG, "run: " +error);
                mMovies.postValue(null);
            }}
                catch (IOException e) {
                e.printStackTrace();
                mMovies.postValue(null);
            }

        }

        private Call<MovieSearchResponse> getSearch(String query, int pageNumber){
            return ServiceGenerator.getMovieApi().searchMovie(
                    Constants.API_KEY,
                    query,
                    String.valueOf(pageNumber)
            );
            }
        private void cancelRequest(){
            Log.d(TAG,"cancel request: the search request is cancelled ");
            cancelRequest = true;
        }

    }

    private class RetrieveMovieRunnable implements Runnable{
        private String movieTitle;

        boolean cancelRequest;

        public RetrieveMovieRunnable(String movieTitle) {
            this.movieTitle = movieTitle;
            cancelRequest = false;

        }

        @Override
        public void run() {
            try {
                Response response = getMovie(movieTitle).execute();
                if(cancelRequest){return;}

                if (response.code() == 200){
                Movie currentMovie = new Movie();
//                    if(pageNumber == 1){
//                        mMovies.postValue(list);
//                    }
//                    else{
//                        List<Movie> currentMovies = mMovies.getValue();
//                        currentMovies.addAll(list);
//                        mMovies.postValue(list);
//                    }

                    currentMovie.setTitle(((MovieResponse)response.body()).getTitle());
                    currentMovie.setDirector(((MovieResponse)response.body()).getDirector());
                    currentMovie.setActors(((MovieResponse)response.body()).getActors());
                    currentMovie.setImdbRating(((MovieResponse)response.body()).getImdbRating());
                    currentMovie.setPlot(((MovieResponse)response.body()).getPlot());
                    currentMovie.setPoster(((MovieResponse)response.body()).getPoster());
                    currentMovie.setYear(((MovieResponse)response.body()).getYear());
                    mMovie.postValue(currentMovie);
                    Log.d(TAG, "run: "+ mMovie.toString());

                }else{
                    String error = response.errorBody().string();
                    Log.e(TAG, "run: " +error);
                    mMovie.postValue(null);
                }}
            catch (IOException e) {
                e.printStackTrace();
                mMovie.postValue(null);
            }

        }

        private Call<MovieResponse> getMovie(String movieTitle){
            return ServiceGenerator.getMovieApi().movieSearch(
                    Constants.API_KEY,
                    movieTitle

            );
        }
        private void cancelRequest(){
            Log.d(TAG,"cancel request: the search request is cancelled ");
            cancelRequest = true;
        }

    }


}
